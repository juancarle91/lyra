<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Contacto;
use App\Categoria;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactoMail;

class ContactoController extends Controller
{

    public function contacto() {

        $categorias = Categoria::all();
        return view ('contacto.contacto', [
            'categorias' => $categorias
        ]);
    }

    public function enviar_mail (Request $request) {

        /*$this->validate ($request, ['nombre'->'required', 'email'->'required', 'contacto'->'required', 'asunto'->'required', 'mensaje'->'required']);*/

        $validarData = $request->validate([
            'nombre' => 'required|unique:posts|max:60',
            'email' => 'required',
            'contacto' => 'required|unique:posts|max:20',
            'asunto' => 'required|unique:posts|max:100',
            'mensaje' => 'required|unique:posts|max:2000',
        ]);
        
        $nombre = $request->input('nombre');
        $email = $request->input('email');
        $contacto = $request->input('contacto');
        $asunto = $request->input('asunto');
        $mensaje = $request->input('mensaje');
        $to_email = 'j.ignaciocarlee@gmail.com';
        $data = [
            'nombre'=>$nombre, 
            'email' => $email, 
            'contacto' => $contacto, 
            'asunto' => $asunto, 
            'mensaje' => $mensaje
        ];
    
        Mail::to($to_email)->send(new ContactoMail($data));

        return redirect('/contacto');

    } 
}