<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $fillable = [
        'titulo', 'descripcion', 'precio', 'foto', 'visible_en', 'categoria_id'
    ];
}
