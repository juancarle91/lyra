@extends('base')


@section ('principal')

<!-- Main Banner Starts -->
<div class="main-banner five">
	<div class="container">
        <h2><span>Contacto</span></h2>
	</div>
</div>
<!-- Main Banner Ends -->
<!-- Breadcrumb Starts -->
<div class="breadcrumb">
	<div class="container">
		<ul class="list-unstyled list-inline">
            <li><a href="/">Home</a></li>
            <li class="active">Contaco</li>
			
		</ul>
	</div>
</div>		
<!-- Breadcrumb Ends -->
<!-- Main Container Starts -->
<div class="container main-container">
<!-- Contact Info Section Starts -->
	<div class="contact-info-box">
		<div class="row">
			<div class="col-md-5 col-xs-12 hidden-sm hidden-xs">
				<div class="box-img">
					<img src="images/contact/contact-info-box-img1.png" alt="Image" />
				</div>
			</div>
			<div class="col-md-6 col-xs-12">
				<div class="info-box">
					<h3>Nos encantaria oir acerca tuyo!</h3>
					<h5>
						Brindamos atencion personalizada durante las 24 hs del dia. Nuestros operadores estaran dispuestos a asesorarlo para que su compra resulte satisfactoria.
					</h5>
					<div class="row">
						<h4 class="col-sm-6 col-xs-12">Tel: 011-1245-4567</h4>
						<h4 class="col-sm-6 col-xs-12">Fax: 011-1245-4567</h4>
					</div>
					<h4>Email: <a href="mailto:fabiana@lyrasalud.com"> fabiana@lyrasalud.com</a></h4>
				</div>
			</div>
			<div class="col-md-1 col-xs-12 hidden-sm hidden-xs"></div>
		</div>
	</div>
<!-- Contact Info Section Ends -->
<!-- Contact Content Starts -->
	<div class="contact-content">
		<div class="row">
		<!-- Contact Form Starts -->
			<div class="col-sm-8 col-xs-12">
				<h3>Realice su consulta mediante el siguiente formulario</h3>
				<div class="status alert alert-success contact-status"></div>
				<form class="contact-form" name="contact-form" method="post" action="/contacto" role="form">
					
					<div class="row">
					<!-- Name Field Starts -->
						<div class="col-md-6">
							<div class="form-group">
								<label for="name">Nombre </label>
								<input type="text" class="form-control" name="nombre" id="name">
							</div>
						</div>
					<!-- Name Field Ends -->
					<!-- Email Field Starts -->
						<div class="col-md-6">
							<div class="form-group">
								<label for="email">Email </label>
								<input type="text" class="form-control" name="email" id="email">
							</div>
						</div>
					<!-- Email Field Ends -->
					<!-- Phone No Field Starts -->
						<div class="col-md-6">
							<div class="form-group">
								<label for="phoneno">Numero de Contacto </label>
								<input type="text" class="form-control" name="contacto" id="phoneno">
							</div>
						</div>
					<!-- Phone No Field Ends -->
					<!-- Subject Field Starts -->
						<div class="col-md-6">
							<div class="form-group">
								<label for="subject">Asunto </label>
								<input type="text" class="form-control" name="asunto" id="subject">
							</div>
						</div>
					<!-- Subject Field Ends -->
					<!-- Message Field Starts -->
						<div class="col-xs-12">
							<div class="form-group">
								<label for="message">Su Consulta: </label>
								<textarea class="form-control" rows="8" name="mensaje" id="message"></textarea>
							</div>
						</div>
					<!-- Message Field Ends -->
						<div class="col-xs-12">
							<input type="submit" class="btn btn-black text-uppercase" value="Enviar" >
						</div>
					</div>
				</form>
			</div>

			@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif

		<!-- Contact Form Ends -->
		<!-- Address Starts -->
			<div class="col-sm-4 col-xs-12">
			<!-- Box #1 Starts -->
				<div class="cblock-1">
					<span class="icon-wrap"><i class="fa fa-car"></i></span>
					<h4>Venga a Visitarnos</h4>
					<ul class="list-unstyled">
						<li>Cabildo, 8011 </li>
						<li>Buenos Aires, Argentina.</li>
						<li>011-1245-4567</li>
					</ul>
				</div>
			<!-- Box #1 Ends -->
			<!-- Box #2 Starts -->
				<div class="cblock-1">
					<span class="icon-wrap red"><i class="fa fa-ambulance"></i></span>
					<h4>Tiene una Emergencia?</h4>
					<ul class="list-unstyled">
						<li>Si usted esta teniendo una emergencia,</li>
						<li>no espere para contactarnos.</li>
						<li>Llame al 011-1245-4567</li>
					</ul>
				</div>
			<!-- Box #2 Ends -->
			</div>
		<!-- Address Ends -->
		</div>
	</div>
<!-- Contact Content Ends -->
</div>
<!-- Main Container Ends -->
<!-- Google Map Starts -->
<div class="map"></div>
<!-- Google Map Ends -->	


@endsection
