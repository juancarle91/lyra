@extends('base')
@section ('principal')
<!-- Main Banner Starts -->
<div class="main-banner seven">
    <div class="container">
        <h2><span>Nuestros Productos</span></h2>
    </div>
</div>
<!-- Main Banner Ends -->
<!-- Breadcrumb Starts -->
<div class="breadcrumb">
    <div class="container">
        <ul class="list-unstyled list-inline">
            <li><a href="/">Home</a></li>
            <li><a href="/productos">Productos</a></li>
            <li class="active">{{$categoria->titulo}}</li>
        </ul>
    </div>
</div>
<!-- Breadcrumb Ends -->
<!-- Main Container Starts -->
<div class="container main-container">
    <!-- Nested Row Starts -->
    <div class="row">
        <!-- Sidearea Starts -->
        <div class="col-sm-3 col-xs-12">
            <!-- Categories Starts -->
            <h4 class="side-heading2 text-bold">Categorias</h4>
            <div class="list-group categories">
                @foreach ($categorias as $cat)
                <a href="#" class="list-group-item">{{ $categoria->titulo }}</a>
                @endforeach
            </div>
            <!-- Categories Ends -->
            <!-- Spacer Starts -->
            <div class="spacer-small"></div>
            <!-- Spacer Ends -->
            <!-- Spacer Starts -->
            <div class="spacer-extra-small"></div>
            <!-- Spacer Ends -->
            <!-- Information Starts -->
            <h4 class="side-heading2 text-bold">Informacion</h4>
            <div class="list-group categories">
                <a href="#" class="list-group-item">Sobre Nosotros</a>
                <a href="#" class="list-group-item">Envios</a>
                <a href="#" class="list-group-item">Terminos y Condiciones</a>
                <a href="#" class="list-group-item">Contacto</a>
            </div>
            <!-- Information Ends -->
        </div>
        <!-- Sidearea Ends -->
        <!-- Mainarea Starts -->
        <div class="col-sm-9 col-xs-12">
            <!-- Page Heading Starts -->
            <h3 class="page-heading1">Productos Odontologicos</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut volutpat rutrum eros amet sollicitudin interdum. Suspendisse pulvinar, velit nec pharetra interdum, ante tellus ornare mi, et mollis tellus neque vitae elit. Mauris adipiscing mauris fringilla turpis interdum sed pulvinar nisi malesuada. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            <!-- Page Heading Ends -->
            <!-- Spacer Starts -->
            <div class="spacer-small"></div>
            <!-- Spacer Ends -->
            <!-- Product Filter Starts -->
            <div class="product-filter">
                
                <!-- Search form -->
                <form class="md-form mt-0" id="buscador" action="/productos/{{ $categoria->id }}" method="get">
                <input class="form-control" type="text" placeholder="Buscar" aria-label="Buscar" name="keyword" value="{{ $keyword }}">
                    <button type="submit" class="btn btn-light"><i class="fas fa-search"></i></button>
                </form>

            </div>
            <!-- Product Filter Ends -->
            <!-- Product Grid Display Starts -->
            <div class="row">
                <!-- Product #1 Starts -->
                @foreach ($productos as $producto)
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="product-col">
                        <!-- Product Image Starts -->
                        <div class="product-col-img">
                            <img src="{{ $producto->foto }}" alt="Product Image" class="img-responsive img-center images-product">
                            <!-- Overlay Starts -->
                            <div class="overlay animation">
                                <!-- Buttons Starts -->
                                <ul class="list-unstyled">
                                    <li><a class="btn btn-block btn-grey" href="#">Agrandar</a></li>
                                    <li><a class="btn btn-block btn-secondary" href="#">Cotizar</a></li>
                                </ul>
                                <!-- Buttons Ends -->
                            </div>
                            <!-- Overlay Ends -->
                        </div>
                        <!-- Product Image Ends -->
                        <h6 class="product-col-name"><a href="#">{{ $producto->titulo }}</a></h6>
                    </div>
                </div>
                @endforeach
                <!-- Product #1 Ends -->
            </div>
            <!-- Product Grid Display Ends -->
            <!-- Spacer Starts -->
            <div class="spacer-medium"></div>
            <!-- Spacer Ends -->
            
            {{ $productos->links()}}

        </div>
        <!-- Mainarea Ends -->
    </div>
    <!-- Nested Row Ends -->
</div>
<!-- Main Container Ends -->
@endsection
