@extends('base')

@section ('principal')

	<!-- Main Banner Starts -->
		<div class="main-banner one">
			<div class="container">
				<h2><span>Sobre Nosotros</span></h2>
			</div>
		</div>
	<!-- Main Banner Ends -->
	<!-- Breadcrumb Starts -->
		<div class="breadcrumb">
			<div class="container">
				<ul class="list-unstyled list-inline">
					<li><a href="/">Home</a></li>
					<li class="active">Sobre Nosotros</li>
				</ul>
			</div>
		</div>		
	<!-- Breadcrumb Ends -->
	<!-- Main Container Starts -->
		<div class="container">
		<!-- About Intro Text Starts -->
			<section class="welcome-area about">
				<div class="row">
					<div class="col-md-6 col-xs-12 about-col">
						<h3 class="main-heading1">Bienvenido a Lyra Salud</h3>
						<h3 class="main-heading2">
							El mejor sitio en cuanto a Insumos de Salud
						</h3>
						<p>
							Lyra Salud es una empresa fundada por , en el ano 1989, y desde entonces se ha encargado de brindar un servicio de primera calidad a todos sus clientes buscando siempre la satisfaccion completa en cuanto al preducto vendido.
						</p>
						<p>
							Actualmente trabajamos con todas las marcas lideres de la industria y tenemso un servicio de envios en 24hs.
						</p>
						<div class="row">
							<div class="col-sm-6 col-xs-12">
								<ul class="list-unstyled list-style-3">
									<li><a href="#">Pediatria</a></li>
									<li><a href="#">Oftalmologias</a></li>
									<li><a href="#">Dermatologia</a></li>
									<li><a href="#">Ortopedia</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-xs-12 hidden-sm hidden-xs">
					</div>
				</div>
			</section>
		<!-- About Intro Text Ends -->
		</div>
	<!-- Main Container Ends -->
	<!-- About Featured Section Starts -->
		<section class="about-featured parallax">
			<div class="container">
				<h2 class="lite">Por Que <span>Nosotros?</span></h2>
				<ul class="list-unstyled list row">
				<!-- List #1 Starts -->
					<li class="col-md-4 col-sm-6 col-xs-12">
						<i class="fa fa-trophy"></i>
						<h4>Eficiencia</h4>
						<p>
							Le enviamos lo que usted necesite en cuando y donde usted lo requiera.
						</p>
					</li>
				<!-- List #1 Ends -->
				<!-- List #2 Starts -->
					<li class="col-md-4 col-sm-6 col-xs-12">
						<i class="fa fa-heartbeat"></i>
						<h4>Atencion al detalle</h4>
						<p>
							Nos ocupamos de todo lo que usted podria solicitar.
						</p>
					</li>
				<!-- List #2 Ends -->
				<!-- List #3 Starts -->
					<li class="col-md-4 col-sm-6 col-xs-12">
						<i class="fa fa-ambulance"></i>
						<h4>Atencion 24 /7</h4>
						<p>
							Brindamos nuestro compromiso de atencion todos los dias del ano 24 hs.
						</p>
					</li>
				<!-- List #3 Ends -->
					<li class="clearfix visible-md"></li>
				<!-- List #4 Starts -->
					<li class="col-md-4 col-sm-6 col-xs-12">
						<i class="fa fa-calendar"></i>
						<h4>Asesoramiento personalizado</h4>
						<p>
							Tabajamos codo a codo con nuestros clientes para brindarles el mejor producto.
						</p>
					</li>
				<!-- List #4 Ends -->
				<!-- List #5 Starts -->
					<li class="col-md-4 col-sm-6 col-xs-12">
						<i class="fa fa-user-md"></i>
						<h4>Ultimas tecnologias</h4>
						<p>
							Nos encontramos en la vanguardia en cuanto a salud, siempre buscando innovacion en las soluciones que brindamos.
						</p>
					</li>
				<!-- List #5 Ends -->
				<!-- List #6 Starts -->
					<li class="col-md-4 col-sm-6 col-xs-12">
						<i class="fa fa-medkit"></i>
						<h4>Profesionalismo</h4>
						<p>
							La salud es nuestra pasion y nuestro equipo asi lo demuestra.
						</p>
					</li>
				<!-- List #6 Ends -->
				</ul>
			</div>
		</section>
	<!-- About Featured Section Ends -->
	
@endsection